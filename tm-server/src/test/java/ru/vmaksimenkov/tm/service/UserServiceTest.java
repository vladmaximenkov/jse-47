package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.util.HashUtil;

public class UserServiceTest extends AbstractTest {

    @NotNull
    private static final String TEST_PASSWORD = "test-password";
    @NotNull
    private static final IUserRecordService USER_SERVICE = BOOTSTRAP.getUserService();
    @NotNull
    private static UserRecord TEST_USER;

    @Test
    @Category(DBCategory.class)
    public void existByEmail() {
        Assert.assertTrue(USER_SERVICE.existsByEmail(TEST_USER_EMAIL));
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        Assert.assertEquals(TEST_USER_ID, USER_SERVICE.findByLogin(TEST_USER_NAME).getId());
    }

    @After
    public void after() {
        new UserRecordRepository(ENTITY_MANAGER).clear();
        ENTITY_MANAGER.getTransaction().commit();
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        USER_SERVICE.removeByLogin(TEST_USER_NAME);
        Assert.assertTrue(USER_SERVICE.findAll().isEmpty());
    }

    @Before
    public void before() {
        ENTITY_MANAGER.getTransaction().begin();
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
    }

}
