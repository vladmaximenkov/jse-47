package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

import javax.persistence.EntityManager;

public class SessionServiceTest extends AbstractTest {

    @NotNull
    private static final ISessionRecordService SESSION_SERVICE = BOOTSTRAP.getSessionService();

    @Nullable
    private static SessionRecord TEST_SESSION;

    @NotNull
    private static String TEST_SESSION_ID;

    @Test
    @Category(DBCategory.class)
    public void checkPassword() {
        Assert.assertTrue(SESSION_SERVICE.checkDataAccess(TEST_USER_NAME, TEST_USER_PASSWORD));
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        SESSION_SERVICE.clear(TEST_USER_ID);
        Assert.assertTrue(SESSION_SERVICE.findAll(TEST_USER_ID).isEmpty());
        SESSION_SERVICE.clear();
        Assert.assertTrue(SESSION_SERVICE.findAll().isEmpty());
    }

    @After
    public void after() {
        SESSION_SERVICE.clear();
        new UserRecordRepository(ENTITY_MANAGER).clear();
        ENTITY_MANAGER.getTransaction().commit();
    }

    @Before
    public void before() {
        ENTITY_MANAGER.getTransaction().begin();
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final SessionRecord session = new SessionRecord();
        TEST_SESSION_ID = session.getId();
        session.setUserId(TEST_USER_ID);
        SESSION_SERVICE.add(session);
        TEST_SESSION = SESSION_SERVICE.findById(TEST_SESSION_ID);
    }

}
