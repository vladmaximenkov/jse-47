package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest extends AbstractTest {

    @NotNull
    protected static final String TEST_PROJECT_NAME = "ProjectRecord test name";

    @NotNull
    protected static final String TEST_PROJECT_NAME_TWO = "Z ProjectRecord test name";

    @NotNull
    private static final IProjectRecordService PROJECT_SERVICE = BOOTSTRAP.getProjectService();

    @Nullable
    private static ProjectRecord TEST_PROJECT;

    @NotNull
    private static String TEST_PROJECT_ID;

    @Test
    @Category(DBCategory.class)
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, PROJECT_SERVICE.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        PROJECT_SERVICE.clear(TEST_USER_ID);
        Assert.assertTrue(PROJECT_SERVICE.findAll(TEST_USER_ID).isEmpty());
        PROJECT_SERVICE.clear();
        Assert.assertTrue(PROJECT_SERVICE.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_PROJECT);
        Assert.assertNotNull(TEST_PROJECT.getId());
        Assert.assertNotNull(TEST_PROJECT.getName());
        Assert.assertEquals(TEST_PROJECT_NAME, TEST_PROJECT.getName());
        Assert.assertNotNull(PROJECT_SERVICE.findAll(TEST_USER_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(PROJECT_SERVICE.existsById(TEST_USER_ID, TEST_PROJECT_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(PROJECT_SERVICE.existsByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        Assert.assertEquals(TEST_PROJECT_ID, PROJECT_SERVICE.findByName(TEST_USER_ID, TEST_PROJECT_NAME).getId());
    }

    @After
    public void after() {
        PROJECT_SERVICE.clear();
        new UserRecordRepository(ENTITY_MANAGER).clear();
        ENTITY_MANAGER.getTransaction().commit();
    }

    private void initTwo() {
        PROJECT_SERVICE.clear(TEST_USER_ID);
        @NotNull final ProjectRecord project1 = new ProjectRecord(TEST_PROJECT_NAME);
        project1.setUserId(TEST_USER_ID);
        project1.setStatus(Status.IN_PROGRESS);
        @NotNull final ProjectRecord project2 = new ProjectRecord(TEST_PROJECT_NAME_TWO);
        project2.setUserId(TEST_USER_ID);
        project2.setStatus(Status.COMPLETE);
        @NotNull final List<ProjectRecord> list = new ArrayList<>();
        list.add(project1);
        list.add(project2);
        PROJECT_SERVICE.add(list);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_PROJECT);
        PROJECT_SERVICE.remove(TEST_PROJECT);
        Assert.assertNull(PROJECT_SERVICE.findById(TEST_PROJECT.getId()));
        Assert.assertTrue(PROJECT_SERVICE.findAll(TEST_USER_ID).isEmpty());
    }

    @Before
    public void before() {
        ENTITY_MANAGER.getTransaction().begin();
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final ProjectRecord project = new ProjectRecord();
        TEST_PROJECT_ID = project.getId();
        project.setName(TEST_PROJECT_NAME);
        project.setUserId(TEST_USER_ID);
        PROJECT_SERVICE.add(project);
        TEST_PROJECT = PROJECT_SERVICE.findById(TEST_USER_ID, project.getId());
    }

}
