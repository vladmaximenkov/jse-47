package ru.vmaksimenkov.tm.api.service.dto;

import ru.vmaksimenkov.tm.api.repository.dto.IAbstractBusinessRecordRepository;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;

public interface IAbstractBusinessRecordService<E extends AbstractBusinessEntityRecord> extends IAbstractBusinessRecordRepository<E> {

}
