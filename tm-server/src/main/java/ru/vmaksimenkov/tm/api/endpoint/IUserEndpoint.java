package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    UserRecord registryUser(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName
    );

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

    @Nullable
    @WebMethod
    UserRecord viewUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

}
