package ru.vmaksimenkov.tm.api;


import ru.vmaksimenkov.tm.endpoint.AbstractEntityRecord;

public interface IService<E extends AbstractEntityRecord> extends IRepository<E> {

}
