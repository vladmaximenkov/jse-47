package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;
import ru.vmaksimenkov.tm.endpoint.Status;
import ru.vmaksimenkov.tm.endpoint.TaskRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void bindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @NotNull
    @WebMethod
    TaskRecord createTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    );

    @Nullable
    @WebMethod
    List<TaskRecord> findTaskAll(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @Nullable
    @WebMethod
    TaskRecord findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    TaskRecord findTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    TaskRecord findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    @WebMethod
    List<TaskRecord> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void setTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void setTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void setTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void unbindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @WebMethod
    void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @WebMethod
    void updateTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "nameNew", partName = "nameNew") @NotNull String nameNew,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

}
