package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.ProjectRecord;

public interface IProjectRepository extends IRepository<ProjectRecord> {

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @Nullable
    ProjectRecord findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectRecord findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    String getIdByName(@NotNull String userId, @NotNull String name);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

}
